public class JavaConditions4 {
    public static void main(String[] args) {
        if (condition) {
            // block of code to be executed if the condition is true
        } else {
            // block of code to be executed if the condition is false
        }
        }
    }
    
}
