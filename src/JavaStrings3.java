public class JavaStrings3 {
    public static void main(String[] args) {
        String txt = "Hello World";
        System.out.println(txt.toUpperCase()); // Outputs "Hello World"
        System.out.println(txt.toLowerCase()); // Outputs "้hello worlds"
    }
    
}
