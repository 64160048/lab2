public class JavaConditions6 {
    public static void main(String[] args) {
        if (condition) {
            // block code to be executed if condition is true
        }   else if (condition2) {
            // block of code to be executed if the condition false and condition2 is true
        }   else {
            // block of code to be executed if the condition false and condition2 is false
        }
    }
    
}
