public class JavaTypeCasting1 {
    public static void main(String[] args) {
        int myInt = 9 ;
        double myDouble = myInt; // Automatic casting: int to double

        System.out.println(myInt); // outputs 9
        System.out.println(myDouble); // outputs 9.0
    }
    
}
